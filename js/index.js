$(function() {
            $("[data-toggle='tooltip']").tooltip();
            $("[data-toggle='popover']").popover();

            $('.carousel').carousel({	
            	interval: 4000	
            });

            $("#contacto").on("show.bs.modal", function (e) {
  				console.log("el modal se esta mostrando");

  				$("#ContactoBtn").removeClass('btn-dark');
  				$("#ContactoBtn").addClass('btn-primary');
  				$("#ContactoBtn").prop('disabled', true);

  				$("#popoverBtn").removeClass('btn-dark');
  				$("#popoverBtn").addClass('btn-primary');
  				$("#popoverBtn").prop('disabled', true);
			});

			 $("#contacto").on("shown.bs.modal", function (e) {
  				console.log("el modal termino de mostrarse");
			});


            $("#contacto").on("hide.bs.modal", function (e) {
  				console.log("el modal se esta ocultando");
			});

			 $("#contacto").on("hidden.bs.modal", function (e) {
  				console.log("el modal termino de ocultarse");

  				$("#ContactoBtn").removeClass('btn-primary');
  				$("#ContactoBtn").addClass('btn-dark');
  				$("#ContactoBtn").prop('disabled', false);			

  				$("#popoverBtn").removeClass('btn-primary');
  				$("#popoverBtn").addClass('btn-dark');
  				$("#popoverBtn").prop('disabled', false);
			});
});